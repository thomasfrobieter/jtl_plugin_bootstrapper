<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">{__('Class name')}</label>\
    <div class="controls">\
        <input{if !empty($widget->Class)} value="{$widget->Class}" {/if} type="text" name="Install-AdminWidget-Widget-Class[]" class="form-control required" placeholder="ExampleClass">\
    </div>\
    <label class="control-label" for="">{__('Title')}</label>\
    <div class="controls">\
        <input{if !empty($widget->Title)} value="{$widget->Title}" {/if} type="text" name="Install-AdminWidget-Widget-Title[]" class="form-control required" placeholder="Eine Überschrift">\
    </div>\
    <label class="control-label" for="">{__('Description')}</label>\
    <div class="controls">\
        <input{if !empty($widget->Description)} value="{$widget->Description}" {/if} type="text" name="Install-AdminWidget-Widget-Description[]" class="form-control required" placeholder="Ein Beispiel-Widget">\
    </div>\
    <label class="control-label" for="">{__('Template file')}</label>\
    <div class="controls">\
        <input{if !empty($widget->TemplateFile)} value="{$widget->TemplateFile}" {/if} type="text" name="Install-AdminWidget-Widget-TemplateFile[]" class="form-control required" placeholder="my_widget.tpl">\
    </div>\
    <label class="control-label" for="">{__('Enabled? (1=yes, 0=no)')}</label>\
    <div class="controls">\
        <input value="{if isset($widget->Active)}{$widget->Active}{else}1{/if}" type="number" min="0" max="1" name="Install-AdminWidget-Widget-Active[]" class="form-control required" placeholder="1">\
    </div>\
    <label class="control-label" for="">{__('Expanded? (1=yes, 0=no)')}</label>\
    <div class="controls">\
        <input value="{if isset($widget->Expanded)}{$widget->Expanded}{else}1{/if}" type="number" min="0" max="1" name="Install-AdminWidget-Widget-Expanded[]" class="form-control required" placeholder="1">\
    </div>\
    <label class="control-label" for="">{__('Container? (center, left, right)')}</label>\
    <div class="controls">\
        <input value="{if isset($widget->Container)}{$widget->Container}{else}center{/if}" type="text" name="Install-AdminWidget-Widget-Container[]" class="form-control required" placeholder="center">\
    </div>\
    <hr>\
</div>\
