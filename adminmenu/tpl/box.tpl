<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">{__('Name')}</label>\
    <div class="controls">\
        <input{if !empty($box->Name)} value="{$box->Name}" {/if} type="text" name="Install-Boxes-Box-Name[]" class="form-control required" placeholder="{__('My great box')}">\
    </div>\
    <label class="control-label" for="">{__('Template file')}</label>\
    <div class="controls">\
        <input{if !empty($box->TemplateFile)} value="{$box->TemplateFile}" {/if} type="text" name="Install-Boxes-Box-TemplateFile[]" class="form-control required" placeholder="my_box.tpl">\
    </div>\
    <label class="control-label" for="">{__('Enable on page types (0 for all page types)')}</label>\
    <div class="controls">\
        <input{if isset($box->Available)} value="{$box->Available}" {/if} type="text" name="Install-Boxes-Box-Available[]" class="form-control required" placeholder="0">\
    </div>\
    <hr>\
</div>\
