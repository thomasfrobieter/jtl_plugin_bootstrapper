<div class="control-group config">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">{__('Name')}</label>\
    <div class="controls">\
        <input{if !empty($setting->Name)} value="{$setting->Name}" {/if} type="text" name="Settingslink-Setting-Name[]" class="form-control required" placeholder="{__('Option no. x')}">\
    </div>\
    <label class="control-label" for="">{__('Config name')}</label>\
    <div class="controls">\
        <input{if !empty($setting->ValueName)} value="{$setting->ValueName}" {/if} type="text" name="Settingslink-Setting-ValueName[]" class="form-control required" placeholder="my_option_x">\
    </div>\
    <label class="control-label" for="">{__('Description')}</label>\
    <div class="controls">\
        <input{if !empty($setting->Description)} value="{$setting->Description}" {/if} type="text" name="Settingslink-Setting-Description[]" class="form-control required" placeholder="{__('Example description')}">\
    </div>\
    <label class="control-label" for="">{__('Initial value')}</label>\
    <div class="controls">\
        <input{if !empty($setting->initialValue)} value="{$setting->initialValue}" {/if} type="text" name="Settingslink-Setting-initialValue[]" class="form-control required" placeholder="42">\
    </div>\
    <label class="control-label" for="">{__('Type')}</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="Settingslink-Setting-type[]">\
            <option{if !empty($setting->type) && $setting->type === 'text'} selected{/if} value="text">Text</option>\
            <option{if !empty($setting->type) && $setting->type === 'selectbox'} selected{/if} value="selectbox">{__('Selectbox')}</option>\
            <option{if !empty($setting->type) && $setting->textarea === 'text'} selected{/if} value="textarea">{__('Textarea')}</option>\
            <option{if !empty($setting->type) && $setting->checkbox === 'text'} selected{/if} value="checkbox">{__('Checkbox')}</option>\
            <option{if !empty($setting->type) && $setting->radio === 'text'} selected{/if} value="radio">{__('Radio')}</option>\
        </select>\
    </div>\
    <div class="option-values-wrap" style="display:none;">\
        <hr>\
        <button class="add-option-values btn btn-primary" type="button">{__('Add item')}</button>\
        <div class="option-values"></div>\
        <hr>\
    </div>\
    <hr>\
</div>\
