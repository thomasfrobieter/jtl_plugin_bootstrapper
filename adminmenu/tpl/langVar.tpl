<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="lang-var-name">{__('Name')}</label>\
    <div class="controls">\
        <input{if !empty($langVar->Name)} value="{$langVar->Name}" {/if} type="text" name="Locales-Variable-Name[]" class="form-control required" placeholder="my_lang_var_x">\
    </div>\
    <label class="control-label" for="lang-var-name">{__('Description')}</label>\
    <div class="controls">\
        <input{if !empty($langVar->Description)} value="{$langVar->Description}" {/if} type="text" name="Locales-Variable-Description[]" class="form-control required" placeholder="{__('My description')}">\
    </div>\
    <label class="control-label" for="name">{__('Value (german)')}</label>\
    <div class="controls">\
        <input{if !empty($langVar->Values.GER)} value="{$langVar->Values.GER}" {/if} type="text" name="Locales-Variable.GER[]" class="form-control required" placeholder="{__('German translation')}">\
    </div>\
    <label class="control-label" for="name">{__('Value (english)')}</label>\
    <div class="controls">\
        <input{if !empty($langVar->Values.ENG)} value="{$langVar->Values.ENG}" {/if} type="text" name="Locales-Variable.ENG[]" class="form-control required" placeholder="{__('English translation')}">\
    </div>\
    <hr>\
</div>\
