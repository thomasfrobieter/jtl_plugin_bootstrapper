<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="hookIDs">{__('Hook')}</label>\
    <div class="controls">\
        <select name="Install-Hooks-Hook-ID[]" class="form-control">\
            {strip}
            {foreach $hookList as $hookData}
                <option{if !empty($hook->id) && $hook->id == $hookData.2} selected{/if} value="{$hookData.2}">{$hookData.1} ({$hookData.2})</option>\
            {/foreach}
            {/strip}
        </select>\
{*        <label class="control-label" for="hookFile">Dateiname</label>\*}
{*        <div class="controls">\*}
{*            <input{if !empty($hook->file)} value="{$hook->file}"{/if} type="text" name="Install-Hooks-Hook[]" class="form-control required" placeholder="meine_hook_datei.php">\*}
{*        </div>\*}
        <label class="control-label" for="hookFile">{__('Priority')}</label>\
        <div class="controls">\
            <input value="{if isset($hook->priority)}{$hook->priority}{else}5{/if}" type="number" name="Install-Hooks-Priority[]" class="form-control required" placeholder=0 (niedrigste) - 9 (höchste)">\
        </div>\
    </div>\
    <hr>\
</div>\
