<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">{__('Name')}</label>\
    <div class="controls">\
        <input{if !empty($customLink->Name)} value="{$customLink->Name}" {/if} type="text" name="Install-Adminmenu-Customlink-Name[]" class="form-control required" placeholder="{__('My tab')}">\
    </div>\
    <label class="control-label" for="">{__('Template name')}</label>\
    <div class="controls">\
        <input{if !empty($customLink->Filename)} value="{$customLink->Filename}" {/if} type="text" name="Install-Adminmenu-Customlink-Filename[]" class="form-control required" placeholder="my_plugin_backend_tab.tpl">\
    </div>\
    <hr>\
</div>\
