<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">{__('File name')}</label>\
    <div class="controls">\
        <input{if !empty($cssFile->name)} value="{$cssFile->name}" {/if} type="text" name="Install-CSS-file-name[]" class="form-control required" placeholder="mycss.css">\
    </div>\
    <label class="control-label" for="">{__('Priority')}</label>\
    <div class="controls">\
        <select name="Install-CSS-file-priority[]" class="form-control required">\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 1} selected {/if} value="1">1 {__('(highest)')}</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 2} selected {/if} value="2">2</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 3} selected {/if} value="3">3</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 4} selected {/if} value="4">4</option>\
            <option{if empty($cssFile->priority) || $cssFile->priority == 5} selected {/if} value="5">5 (Standard)</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 6} selected {/if} value="6">6</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 7} selected {/if} value="7">7</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 9} selected {/if} value="8">8</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 9} selected {/if} value="9">9</option>\
            <option{if !empty($cssFile->priority) && $cssFile->priority == 10} selected {/if} value="10">10 {__('(lowest)')}</option>\
        </select>\
    </div>\
    <hr>\
</div>\
